package json_message_writer

import java.io.{BufferedWriter, FileWriter}
import java.util.concurrent.atomic.AtomicInteger

import org.slf4j.{Logger, LoggerFactory}
import play.api.libs.json.JsValue


class JsonToFileWriter(val stream: LazyList[JsValue], filePath: String, messagesToWrite: Int) {
  val logger: Logger = LoggerFactory.getLogger(s"json-writer")
  val writer = new BufferedWriter(new FileWriter(filePath, true))

  private def write(cnt: AtomicInteger, json: String): Unit = {
    cnt.getAndIncrement()
    writer.write(json)
    writer.append("\n")
  }

  def writeJsonStream(counter: AtomicInteger = new AtomicInteger()): Unit = {
    try {
      stream foreach { json =>
        if (counter.get() < messagesToWrite) { write(counter, json.toString) }
        else closeWriter(counter)
      }
    } catch {
      case e: Exception => logger.debug(s"$e is happened"); System.exit(2)
    }
    finally {
      closeWriter(counter)
    }
  }

  def closeWriter(counter: AtomicInteger) = {
    writer.close()
    logger.info(s"$counter rows were written to path: $filePath")
    System.exit(0)
  }
}
