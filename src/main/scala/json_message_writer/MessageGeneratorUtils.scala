package json_message_writer

import play.api.libs.json.{JsValue, Json, OWrites}

import scala.util.Random

trait MessageGeneratorUtils {
  private val r = Random
  val residentReads: OWrites[MessageTemplate] = Json.writes[MessageTemplate]

  def randomPrintableCharArray(length: Int, list: List[Char]): List[Char] = {
    if (length == 1) r.nextPrintableChar() :: list
    else randomPrintableCharArray(length - 1, r.nextPrintableChar() :: list)
  }

  def generateValues(): LazyList[(Long, String, Int)] = {
    LazyList.continually((r.nextLong(100000), randomPrintableCharArray(10, Nil).mkString(""), r.nextInt(100000)))
  }
}

trait JsonUtils {
  implicit val messageTemplateWrites = Json.writes[MessageTemplate]

  def returnJsonValue(values: MessageTemplate): JsValue = {
    Json.toJson(values)
  }

}