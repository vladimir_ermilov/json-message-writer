package json_message_writer


case class MessageTemplate(id: Long, itemName: String, itemNum: Int)

object MessageTemplate {
  def apply(id: Long, itemName: String, itemNum: Int): MessageTemplate =
    new MessageTemplate(id, itemName, itemNum)

  def tupled = (MessageTemplate.apply _).tupled
}