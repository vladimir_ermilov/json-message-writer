package json_message_writer


object JsonMessageWriter extends MessageGeneratorUtils with JsonUtils {
  def main(args: Array[String]): Unit = {
    val conf = new Conf(args)

    def stream = generateValues
      .map(MessageTemplate.tupled)
      .map(returnJsonValue)

    val randomJsonWriter = new JsonToFileWriter(stream, conf.filePath(), conf.messagesToWrite())
    randomJsonWriter.writeJsonStream()
  }
}