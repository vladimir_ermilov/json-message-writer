package json_message_writer

import org.rogach.scallop._

class Conf(arguments: Seq[String]) extends ScallopConf(arguments) {
  val filePath = opt[String](required = true)
  val messagesToWrite = opt[Int](required = true)
  verify()
}